using System;
using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Abstractions.Delegates;
using Standalone.Cqrs.Abstractions.Pipelines;

namespace Standalone.Cqrs.Middlewares
{
    internal sealed class ExceptionHandlingMiddleware<TSource, TResult> : IExecutionMiddleware<TSource, TResult>
        where TSource : ISource<TResult>
        where TResult : ISourceResult
    {
        public async Task Execute(IExecutionContext<TSource, TResult> context,
            SourceExecutionDelegate<TSource, TResult> next)
        {
            try
            {
                await next(context);
            }
            catch (Exception exception)
            {
                context.CompleteWithException(exception);
            }
        }
    }
}