using System;
using System.Reflection;
using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Attributes;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Abstractions.Delegates;
using Standalone.Cqrs.Abstractions.Exceptions;
using Standalone.Cqrs.Abstractions.Pipelines;

namespace Standalone.Cqrs.Middlewares
{
    internal sealed class TimeoutMiddleware<TSource, TResult> : IExecutionMiddleware<TSource, TResult>
        where TSource : ISource<TResult>
        where TResult : ISourceResult
    {
        private readonly TimeSpan? _timeout;

        public TimeoutMiddleware()
        {
            var timeoutAttribute = typeof(TSource).GetCustomAttribute<ExecutionTimeoutAttribute>();

            _timeout = timeoutAttribute?.Timeout;
        }

        public async Task Execute(IExecutionContext<TSource, TResult> context,
            SourceExecutionDelegate<TSource, TResult> next)
        {
            var timeout = _timeout ?? context.Timeout;

            context.CancellationTokenSource.CancelAfter(timeout);

            var timeoutTask = Task.Delay(timeout);
            var nextTask = next(context);

            var resultTask = await Task.WhenAny(nextTask, timeoutTask);

            if (resultTask == timeoutTask)
            {
                throw new SourceExecutionTimeoutException(context.SourceType, context.ResultType, timeout);
            }

            await resultTask;
        }
    }
}