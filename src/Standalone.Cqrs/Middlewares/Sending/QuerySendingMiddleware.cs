using System;
using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Abstractions.Delegates;
using Standalone.Cqrs.Abstractions.Pipelines;
using Standalone.Cqrs.Transport.Abstractions.Exceptions;
using Standalone.Cqrs.Transport.Abstractions.Senders;

namespace Standalone.Cqrs.Middlewares.Sending
{
    internal sealed class QuerySendingMiddleware<TQuery, TResult> : IExecutionMiddleware<TQuery, TResult>
        where TQuery : IQuery<TResult>
        where TResult : IQueryResult
    {
        public async Task Execute(IExecutionContext<TQuery, TResult> context,
            SourceExecutionDelegate<TQuery, TResult> next)
        {
            if (context.IsCompleted)
            {
                await next(context);
                return;
            }

            var transport = context.ServiceScope.GetService<IQueryContextSender>(isOptional: true);

            if (transport == null)
            {
                throw new NoSuitableTransportFoundException(context.SourceType, context.ResultType);
            }

            await transport.Send(context);
            await next(context);
        }
    }
}