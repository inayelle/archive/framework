using System;
using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Abstractions.Delegates;
using Standalone.Cqrs.Abstractions.Pipelines;
using Standalone.Cqrs.Transport.Abstractions.Exceptions;
using Standalone.Cqrs.Transport.Abstractions.Senders;

namespace Standalone.Cqrs.Middlewares.Sending
{
    internal sealed class CommandSendingMiddleware<TCommand> : IExecutionMiddleware<TCommand, VoidResult>
        where TCommand : ICommand
    {
        public async Task Execute(IExecutionContext<TCommand, VoidResult> context,
            SourceExecutionDelegate<TCommand, VoidResult> next)
        {
            if (context.IsCompleted)
            {
                await next(context);
                return;
            }

            var transport = context.ServiceScope.GetService<ICommandContextSender>(isOptional: true);

            if (transport == null)
            {
                throw new NoSuitableTransportFoundException(context.SourceType, context.ResultType);
            }

            await transport.Send(context);
            await next(context);
        }
    }

    internal sealed class CommandSendingMiddleware<TCommand, TResult> : IExecutionMiddleware<TCommand, TResult>
        where TCommand : ICommand<TResult>
        where TResult : ICommandResult
    {
        public async Task Execute(IExecutionContext<TCommand, TResult> context,
            SourceExecutionDelegate<TCommand, TResult> next)
        {
            if (context.IsCompleted)
            {
                await next(context);
                return;
            }

            var transport = context.ServiceScope.GetService<ICommandContextSender>(isOptional: true);

            if (transport == null)
            {
                throw new NoSuitableTransportFoundException(context.SourceType, context.ResultType);
            }

            await transport.Send(context);
            await next(context);
        }
    }
}