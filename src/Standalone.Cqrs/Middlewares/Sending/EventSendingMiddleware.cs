using System;
using System.Linq;
using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Abstractions.Delegates;
using Standalone.Cqrs.Abstractions.Pipelines;
using Standalone.Cqrs.Transport.Abstractions.Senders;

namespace Standalone.Cqrs.Middlewares.Sending
{
    internal sealed class EventSendingMiddleware<TEvent> : IExecutionMiddleware<TEvent, VoidResult>
        where TEvent : IEvent
    {
        public async Task Execute(IExecutionContext<TEvent, VoidResult> context,
            SourceExecutionDelegate<TEvent, VoidResult> next)
        {
            var eventTransports = context.ServiceScope.GetServices<IEventContextSender>();

            var tasks = eventTransports.Select(transport => transport.Publish(context));

            try
            {
                await Task.WhenAll(tasks);
                context.CompleteWithResult(VoidResult.Instance);
            }
            catch (Exception exception)
            {
                context.CompleteWithException(exception);
            }
            
            await next(context);
        }
    }
}