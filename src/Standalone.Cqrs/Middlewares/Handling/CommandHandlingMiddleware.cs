using System;
using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Abstractions.Delegates;
using Standalone.Cqrs.Abstractions.Handlers;
using Standalone.Cqrs.Abstractions.Pipelines;

namespace Standalone.Cqrs.Middlewares.Handling
{
    internal sealed class CommandHandlingMiddleware<TCommand> : IExecutionMiddleware<TCommand, VoidResult>
        where TCommand : ICommand
    {
        public async Task Execute(IExecutionContext<TCommand, VoidResult> context,
            SourceExecutionDelegate<TCommand, VoidResult> next)
        {
            var handler = context.ServiceScope.GetService<ICommandHandler<TCommand>>(isOptional: true);

            if (handler == null)
            {
                await next(context);
                return;
            }

            await handler.Handle(context.Source, context.CancellationToken);
            context.CompleteWithResult(VoidResult.Instance);
        }
    }

    internal sealed class CommandHandlingMiddleware<TCommand, TResult> : IExecutionMiddleware<TCommand, TResult>
        where TCommand : ICommand<TResult>
        where TResult : ICommandResult
    {
        public async Task Execute(IExecutionContext<TCommand, TResult> context,
            SourceExecutionDelegate<TCommand, TResult> next)
        {
            var handler = context.ServiceScope.GetService<ICommandHandler<TCommand, TResult>>(isOptional: true);

            if (handler == null)
            {
                await next(context);
                return;
            }

            var result = await handler.Handle(context.Source, context.CancellationToken);
            context.CompleteWithResult(result);
        }
    }
}