using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Abstractions.Delegates;
using Standalone.Cqrs.Abstractions.Handlers;
using Standalone.Cqrs.Abstractions.Pipelines;

namespace Standalone.Cqrs.Middlewares.Handling
{
    internal sealed class EventHandlingMiddleware<TEvent> : IExecutionMiddleware<TEvent, VoidResult>
        where TEvent : IEvent
    {
        public async Task Execute(IExecutionContext<TEvent, VoidResult> context,
            SourceExecutionDelegate<TEvent, VoidResult> next)
        {
            var serviceScope = context.ServiceScope;
            var handlers = serviceScope.GetServices<IEventHandler<TEvent>>();

            // TODO: log it
            // ReSharper disable once CollectionNeverQueried.Local
            var caughtExceptions = new List<Exception>();

            foreach (var handler in handlers)
            {
                try
                {
                    await handler.Handle(context.Source, context.CancellationToken);
                }
                catch (Exception exception)
                {
                    caughtExceptions.Add(exception);
                }
            }

            await next(context);
        }
    }
}