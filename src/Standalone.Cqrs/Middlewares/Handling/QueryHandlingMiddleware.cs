using System;
using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Abstractions.Delegates;
using Standalone.Cqrs.Abstractions.Handlers;
using Standalone.Cqrs.Abstractions.Pipelines;

namespace Standalone.Cqrs.Middlewares.Handling
{
    internal sealed class QueryHandlingMiddleware<TQuery, TResult> : IExecutionMiddleware<TQuery, TResult>
        where TQuery : IQuery<TResult>
        where TResult : IQueryResult
    {
        public async Task Execute(IExecutionContext<TQuery, TResult> context,
            SourceExecutionDelegate<TQuery, TResult> next)
        {
            var serviceScope = context.ServiceScope;

            var handler = serviceScope.GetService<IQueryHandler<TQuery, TResult>>(isOptional: true);

            if (handler == null)
            {
                await next(context);
                return;
            }

            var result = await handler.Handle(context.Source, context.CancellationToken);
            context.CompleteWithResult(result);
        }
    }
}