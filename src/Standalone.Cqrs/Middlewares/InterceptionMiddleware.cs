using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Abstractions.Delegates;
using Standalone.Cqrs.Abstractions.Interception;
using Standalone.Cqrs.Abstractions.Pipelines;

namespace Standalone.Cqrs.Middlewares
{
    internal sealed class InterceptionMiddleware<TSource, TResult> : IExecutionMiddleware<TSource, TResult>
        where TSource : ISource<TResult>
        where TResult : ISourceResult
    {
        public async Task Execute(IExecutionContext<TSource, TResult> context,
            SourceExecutionDelegate<TSource, TResult> next)
        {
            var serviceScope = context.ServiceScope;
            var interceptors = serviceScope.GetServices<ISourceInterceptor>();

            foreach (var interceptor in interceptors)
            {
                await interceptor.InterceptBefore(context);
            }

            await next(context);

            foreach (var interceptor in interceptors)
            {
                await interceptor.InterceptAfter(context);
            }
        }
    }
}