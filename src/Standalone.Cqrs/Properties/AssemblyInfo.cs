using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Standalone.Cqrs.Transport.Local")]
[assembly: InternalsVisibleTo("Standalone.Cqrs.Transport.RabbitMq")]
[assembly: InternalsVisibleTo("Standalone.Cqrs.Transport.Http")]
[assembly: InternalsVisibleTo("Standalone.Cqrs.DependencyInjection.Autofac")]
[assembly: InternalsVisibleTo("Standalone.Cqrs.DependencyInjection.Microsoft")]