using System;
using System.Threading;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Abstractions.Options;
using Standalone.DependencyInjection.Abstractions;

namespace Standalone.Cqrs.Context
{
    internal sealed class ExecutionContext<TSource, TResult> : IExecutionContext<TSource, TResult>
        where TSource : ISource<TResult> 
        where TResult : ISourceResult
    {
        public Guid CorrelationId { get; }

        public bool IsCompleted => IsCompletedSuccessfully | IsCompletedFaulty;
        public bool IsCompletedSuccessfully => Result != null;
        public bool IsCompletedFaulty => Exception != null;

        public TimeSpan Timeout { get; }
        public TSource Source { get; }
        public TResult Result { get; private set; }
        public Exception Exception { get; private set; }

        public CancellationTokenSource CancellationTokenSource { get; }
        public IServiceScope ServiceScope { get; }

        public ExecutionContext(TSource source, IServiceScope serviceScope, IExecutionOptions options,
            CancellationToken ctoken, Guid correlationId)
        {
            CorrelationId = correlationId;
            CancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(ctoken);

            Source = source;
            ServiceScope = serviceScope;
            Timeout = options.ExecutionTimeout;
        }

        public void CompleteWithResult(TResult result)
        {
            Exception = default;
            Result = result;
        }

        public void CompleteWithException(Exception exception)
        {
            Result = default;
            Exception = exception;
        }

        public void Dispose()
        {
            CancellationTokenSource?.Dispose();
            ServiceScope?.Dispose();
        }
    }
}