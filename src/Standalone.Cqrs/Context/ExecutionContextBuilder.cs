using System;
using System.Threading;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Abstractions.Options;
using Standalone.DependencyInjection.Abstractions;

namespace Standalone.Cqrs.Context
{
    internal sealed class ExecutionContextBuilder : IExecutionContextBuilder
    {
        private readonly IExecutionOptions _options;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        private CancellationToken? _cancellationToken;
        private Guid? _correlationId;
        private IServiceScope _serviceScope;

        public ExecutionContextBuilder(IExecutionOptions options, IServiceScopeFactory serviceScopeFactory)
        {
            _options = options;
            _serviceScopeFactory = serviceScopeFactory;
        }

        public IExecutionContextBuilder UseCancellationToken(CancellationToken ctoken)
        {
            _cancellationToken = ctoken;

            return this;
        }

        public IExecutionContextBuilder UseCorrelationId(Guid correlationId)
        {
            _correlationId = correlationId;

            return this;
        }

        public IExecutionContextBuilder UseServiceScope(IServiceScope serviceScope)
        {
            _serviceScope = serviceScope;

            return this;
        }

        public IExecutionContext<TSource, TResult> Build<TSource, TResult>(TSource source)
            where TSource : ISource<TResult>
            where TResult : ISourceResult
        {
            var serviceScope = _serviceScope ?? _serviceScopeFactory.Create();
            var ctoken = _cancellationToken ?? CancellationToken.None;
            var correlationId = _correlationId ?? Guid.NewGuid();
            
            return new ExecutionContext<TSource,TResult>(source, serviceScope, _options, ctoken, correlationId);
        }
    }
}