using System;
using System.Threading;
using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Buses;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Abstractions.Exceptions;
using Standalone.Cqrs.Abstractions.Pipelines;
using Standalone.DependencyInjection.Abstractions;

namespace Standalone.Cqrs.Buses
{
    internal sealed class EventBus : IEventBus
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public EventBus(IServiceScopeFactory serviceScopeFactory)
        {
            _serviceScopeFactory = serviceScopeFactory;
        }

        public Task Publish<TEvent>(TEvent @event, CancellationToken ctoken) where TEvent : IEvent
        {
            return Publish(@event, Guid.NewGuid(), ctoken);
        }

        public async Task Publish<TEvent>(TEvent @event, Guid correlationId, CancellationToken ctoken)
            where TEvent : IEvent
        {
            ThreadPool.QueueUserWorkItem(async state =>
            {
                await Task.Yield();
                
                using var serviceScope = _serviceScopeFactory.Create();

                var contextBuilder = serviceScope.GetService<IExecutionContextBuilder>();

                using var context = contextBuilder
                   .UseServiceScope(serviceScope)
                   .UseCorrelationId(correlationId)
                   .UseCancellationToken(ctoken)
                   .Build<TEvent, VoidResult>(@event);

                var pipeline = context.ServiceScope.GetService<IExecutionPipeline<TEvent, VoidResult>>();

                await pipeline.Execute(context);

                if (!context.IsCompleted)
                {
                    throw new IncompletedExecutionContextException();
                }
            });
        }
    }
}