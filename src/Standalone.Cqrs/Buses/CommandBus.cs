using System;
using System.Threading;
using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Buses;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Abstractions.Exceptions;
using Standalone.Cqrs.Abstractions.Pipelines;
using Standalone.DependencyInjection.Abstractions;

namespace Standalone.Cqrs.Buses
{
    internal sealed class CommandBus : ICommandBus
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public CommandBus(IServiceScopeFactory serviceScopeFactory)
        {
            _serviceScopeFactory = serviceScopeFactory;
        }

        public Task Execute<TCommand>(TCommand command, CancellationToken ctoken) where TCommand : ICommand
        {
            return Execute(command, Guid.NewGuid(), ctoken);
        }

        public async Task Execute<TCommand>(TCommand command, Guid correlationId, CancellationToken ctoken)
            where TCommand : ICommand
        {
            await Task.Yield();
            
            var serviceScope = _serviceScopeFactory.Create();

            var contextBuilder = serviceScope.GetService<IExecutionContextBuilder>();
            
            using var context = contextBuilder
               .UseServiceScope(serviceScope)
               .UseCorrelationId(correlationId)
               .UseCancellationToken(ctoken)
               .Build<TCommand, VoidResult>(command);

            var pipeline = context.ServiceScope.GetService<IExecutionPipeline<TCommand, VoidResult>>();

            await pipeline.Execute(context);

            if (!context.IsCompleted)
            {
                throw new IncompletedExecutionContextException();
            }

            if (context.IsCompletedFaulty)
            {
                throw context.Exception;
            }
        }

        public Task<TResult> Execute<TCommand, TResult>(TCommand command, CancellationToken ctoken)
            where TCommand : ICommand<TResult>
            where TResult : ICommandResult
        {
            return Execute<TCommand, TResult>(command, Guid.NewGuid(), ctoken);
        }

        public async Task<TResult> Execute<TCommand, TResult>(TCommand command, Guid correlationId,
            CancellationToken ctoken)
            where TCommand : ICommand<TResult>
            where TResult : ICommandResult
        {
            await Task.Yield();
            
            using var serviceScope = _serviceScopeFactory.Create();

            var contextBuilder = serviceScope.GetService<IExecutionContextBuilder>();

            using var context = contextBuilder
               .UseServiceScope(serviceScope)
               .UseCorrelationId(correlationId)
               .UseCancellationToken(ctoken)
               .Build<TCommand, TResult>(command);

            var pipeline = context.ServiceScope.GetService<IExecutionPipeline<TCommand, TResult>>();

            await pipeline.Execute(context);

            if (!context.IsCompleted)
            {
                throw new IncompletedExecutionContextException();
            }

            if (context.IsCompletedFaulty)
            {
                throw context.Exception;
            }

            return context.Result;
        }
    }
}