using System;
using System.Threading;
using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Buses;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Abstractions.Exceptions;
using Standalone.Cqrs.Abstractions.Pipelines;
using Standalone.DependencyInjection.Abstractions;

namespace Standalone.Cqrs.Buses
{
    internal sealed class QueryBus : IQueryBus
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public QueryBus(IServiceScopeFactory serviceScopeFactory)
        {
            _serviceScopeFactory = serviceScopeFactory;
        }

        public Task<TResult> Perform<TQuery, TResult>(TQuery query, CancellationToken ctoken)
            where TQuery : IQuery<TResult>
            where TResult : IQueryResult
        {
            return Perform<TQuery, TResult>(query, Guid.NewGuid(), ctoken);
        }

        public async Task<TResult> Perform<TQuery, TResult>(TQuery query, Guid correlationId, CancellationToken ctoken)
            where TQuery : IQuery<TResult>
            where TResult : IQueryResult
        {
            await Task.Yield();
            
            using var serviceScope = _serviceScopeFactory.Create();

            var contextBuilder = serviceScope.GetService<IExecutionContextBuilder>();
            
            using var context = contextBuilder
               .UseServiceScope(serviceScope)
               .UseCorrelationId(correlationId)
               .UseCancellationToken(ctoken)
               .Build<TQuery, TResult>(query);

            var pipeline = context.ServiceScope.GetService<IExecutionPipeline<TQuery, TResult>>();

            await pipeline.Execute(context);

            if (!context.IsCompleted)
            {
                throw new IncompletedExecutionContextException();
            }

            if (context.IsCompletedFaulty)
            {
                throw context.Exception;
            }

            return context.Result;
        }
    }
}