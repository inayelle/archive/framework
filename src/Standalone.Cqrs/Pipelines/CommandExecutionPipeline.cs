using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Middlewares;
using Standalone.Cqrs.Middlewares.Handling;
using Standalone.Cqrs.Middlewares.Sending;

namespace Standalone.Cqrs.Pipelines
{
    internal sealed class CommandExecutionPipeline<TCommand> : ExecutionPipelineBase<TCommand, VoidResult>
        where TCommand : ICommand
    {
        public CommandExecutionPipeline()
        {
            AddMiddleware(new ExceptionHandlingMiddleware<TCommand, VoidResult>());
            AddMiddleware(new TimeoutMiddleware<TCommand, VoidResult>());
            AddMiddleware(new InterceptionMiddleware<TCommand, VoidResult>());
            AddMiddleware(new CommandHandlingMiddleware<TCommand>());
            AddMiddleware(new CommandSendingMiddleware<TCommand>());
        }
    }

    internal sealed class CommandExecutionPipeline<TCommand, TResult> : ExecutionPipelineBase<TCommand, TResult>
        where TCommand : ICommand<TResult>
        where TResult : ICommandResult
    {
        public CommandExecutionPipeline()
        {
            AddMiddleware(new ExceptionHandlingMiddleware<TCommand, TResult>());
            AddMiddleware(new TimeoutMiddleware<TCommand, TResult>());
            AddMiddleware(new InterceptionMiddleware<TCommand, TResult>());
            AddMiddleware(new CommandHandlingMiddleware<TCommand, TResult>());
            AddMiddleware(new CommandSendingMiddleware<TCommand, TResult>());
        }
    }
}