using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Middlewares;
using Standalone.Cqrs.Middlewares.Handling;
using Standalone.Cqrs.Middlewares.Sending;

namespace Standalone.Cqrs.Pipelines
{
    internal sealed class EventExecutionPipeline<TEvent> : ExecutionPipelineBase<TEvent, VoidResult>
        where TEvent : IEvent
    {
        public EventExecutionPipeline()
        {
            AddMiddleware(new ExceptionHandlingMiddleware<TEvent, VoidResult>());
            AddMiddleware(new EventHandlingMiddleware<TEvent>());
            AddMiddleware(new EventSendingMiddleware<TEvent>());
        }
    }
}