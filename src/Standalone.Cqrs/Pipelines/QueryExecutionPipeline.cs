using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Middlewares;
using Standalone.Cqrs.Middlewares.Handling;
using Standalone.Cqrs.Middlewares.Sending;

namespace Standalone.Cqrs.Pipelines
{
    internal sealed class QueryExecutionPipeline<TQuery, TResult> : ExecutionPipelineBase<TQuery, TResult>
        where TQuery : IQuery<TResult>
        where TResult : IQueryResult
    {
        public QueryExecutionPipeline()
        {
            AddMiddleware(new ExceptionHandlingMiddleware<TQuery, TResult>());
            AddMiddleware(new TimeoutMiddleware<TQuery, TResult>());
            AddMiddleware(new InterceptionMiddleware<TQuery, TResult>());
            AddMiddleware(new QueryHandlingMiddleware<TQuery, TResult>());
            AddMiddleware(new QuerySendingMiddleware<TQuery, TResult>());
        }
    }
}