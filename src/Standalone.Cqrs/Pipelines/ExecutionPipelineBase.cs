using System.Collections.Generic;
using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Abstractions.Delegates;
using Standalone.Cqrs.Abstractions.Pipelines;

namespace Standalone.Cqrs.Pipelines
{
    internal abstract class ExecutionPipelineBase<TSource, TResult> : IExecutionPipeline<TSource, TResult>
        where TSource : ISource<TResult>
        where TResult : ISourceResult
    {
        private readonly IList<SourceExecutionChainDelegate<TSource, TResult>> _chain;

        protected ExecutionPipelineBase()
        {
            _chain = new List<SourceExecutionChainDelegate<TSource, TResult>>(5);
        }

        public async Task Execute(IExecutionContext<TSource, TResult> context)
        {
            SourceExecutionDelegate<TSource, TResult> chain = ctx => Task.CompletedTask;

            for (int index = _chain.Count - 1; index >= 0; --index)
            {
                chain = _chain[index](chain);
            }

            await chain(context);
        }

        protected void AddMiddleware<TMiddleware>(TMiddleware middleware)
            where TMiddleware : IExecutionMiddleware<TSource, TResult>
        {
            SourceExecutionChainDelegate<TSource, TResult> @delegate = next =>
            {
                return async context =>
                {
                    await middleware.Execute(context, next);
                };
            };
            
            _chain.Add(@delegate);
        }
    }
}