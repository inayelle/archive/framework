using Autofac;
using Standalone.DependencyInjection.Abstractions;

namespace Standalone.DependencyInjection.Adapters.Autofac.Modules
{
    internal sealed class AdapterModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AutofacServiceScopeFactory>()
               .As<IServiceScopeFactory>()
               .InstancePerLifetimeScope();

            builder.Register(context =>
                {
                    var factory = context.Resolve<IServiceScopeFactory>();

                    return factory.Create();
                })
               .As<IServiceScope>()
               .InstancePerLifetimeScope();
        }
    }
}