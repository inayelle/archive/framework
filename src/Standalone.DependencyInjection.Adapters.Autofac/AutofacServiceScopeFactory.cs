using Autofac;
using Standalone.DependencyInjection.Abstractions;

namespace Standalone.DependencyInjection.Adapters.Autofac
{
    internal sealed class AutofacServiceScopeFactory : IServiceScopeFactory
    {
        private readonly ILifetimeScope _lifetimeScope;

        public AutofacServiceScopeFactory(ILifetimeScope lifetimeScope)
        {
            _lifetimeScope = lifetimeScope;
        }

        public IServiceScope Create()
        {
            var autofacServiceScope = new AutofacServiceScope(_lifetimeScope.BeginLifetimeScope());

            return autofacServiceScope;
        }
    }
}