using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Standalone.DependencyInjection.Abstractions;

namespace Standalone.DependencyInjection.Adapters.Autofac
{
    internal sealed class AutofacServiceScope : IServiceScope
    {
        private readonly ILifetimeScope _lifetimeScope;
        private bool _disposed;

        public AutofacServiceScope(ILifetimeScope lifetimeScope)
        {
            _lifetimeScope = lifetimeScope;
            _disposed = false;
        }

        public TService GetService<TService>(bool isOptional = false) where TService : class
        {
            var service = _lifetimeScope.ResolveOptional<TService>();

            if (service == null && isOptional == false)
            {
                throw new InvalidOperationException();
            }

            return service;
        }

        public TService GetService<TService>(object key, bool isOptional = false) where TService : class
        {
            var service = _lifetimeScope.ResolveOptionalKeyed<TService>(key);

            if (service == null && isOptional == false)
            {
                throw new InvalidOperationException();
            }

            return service;
        }

        public object GetService(Type serviceType, bool isOptional = false)
        {
            var service = _lifetimeScope.ResolveOptional(serviceType);

            if (service == null && isOptional == false)
            {
                throw new InvalidOperationException();
            }

            return service;
        }

        public bool HasService<TService>()
        {
            return _lifetimeScope.IsRegistered<TService>();
        }

        public bool HasService(Type serviceType)
        {
            return _lifetimeScope.IsRegistered(serviceType);
        }

        public IReadOnlyCollection<TService> GetServices<TService>()
        {
            return _lifetimeScope.Resolve<IEnumerable<TService>>().ToList();
        }

        public IReadOnlyCollection<object> GetServices(Type serviceType)
        {
            var enumerable = _lifetimeScope.Resolve(typeof(IEnumerable<>).MakeGenericType(serviceType));

            return ((IEnumerable<object>) enumerable).ToList();
        }

        public void Dispose()
        {
            if (_disposed)
            {
                return;
            }
            
            _lifetimeScope?.Dispose();
            _disposed = true;
        }
    }
}