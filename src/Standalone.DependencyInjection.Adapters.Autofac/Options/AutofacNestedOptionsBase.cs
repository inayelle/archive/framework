using System;
using Autofac;

namespace Standalone.DependencyInjection.Adapters.Autofac.Options
{
    public abstract class AutofacNestedOptionsBase<TBaseOptions> : AutofacRootOptionsBase 
        where TBaseOptions : AutofacRootOptionsBase
    {
        private readonly TBaseOptions _baseOptions;

        protected AutofacNestedOptionsBase(TBaseOptions baseOptions)
        {
            _baseOptions = baseOptions;
        }

        public override void RegisterModule<TModule>()
        {
            _baseOptions.RegisterModule<TModule>();
        }

        public override void RegisterModule<TModule>(TModule module)
        {
            _baseOptions.RegisterModule(module);
        }

        public override void ApplyModules(ContainerBuilder builder)
        {
            throw new InvalidOperationException();
        }
    }
}