using System.Collections.Generic;
using Autofac;
using Autofac.Core;

namespace Standalone.DependencyInjection.Adapters.Autofac.Options
{
    public abstract class AutofacRootOptionsBase
    {
        private readonly List<IModule> _modules;

        protected AutofacRootOptionsBase()
        {
            _modules = new List<IModule>();
        }

        public virtual void RegisterModule<TModule>() where TModule : IModule, new()
        {
            RegisterModule(new TModule());
        }

        public virtual void RegisterModule<TModule>(TModule module) where TModule : IModule
        {
            _modules.Add(module);
        }

        public virtual void ApplyModules(ContainerBuilder builder)
        {
            foreach (var module in _modules)
            {
                builder.RegisterModule(module);
            }
        }
    }
}