using System;
using Autofac;
using Standalone.DependencyInjection.Adapters.Autofac.Modules;
using Standalone.DependencyInjection.Adapters.Autofac.Options;

namespace Standalone.DependencyInjection.Adapters.Autofac.Extensions
{
    public static class StandaloneAutofacOptionsExtensions
    {
        public static ContainerBuilder RegisterStandaloneAdapter(this ContainerBuilder builder,
            Action<StandaloneAutofacAdapterOptions> configure = null)
        {
            var options = new StandaloneAutofacAdapterOptions();

            options.RegisterModule<AdapterModule>();
            
            configure?.Invoke(options);
            
            options.ApplyModules(builder);

            return builder;
        }
    }
}