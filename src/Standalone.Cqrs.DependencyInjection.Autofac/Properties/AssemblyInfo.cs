using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Standalone.Cqrs.Extensions.FluentValidation.Autofac")]

[assembly: InternalsVisibleTo("Standalone.Cqrs.Transport.Local.Autofac")]
[assembly: InternalsVisibleTo("Standalone.Cqrs.Transport.RabbitMq.Autofac")]