using System.Reflection;
using Standalone.Cqrs.Abstractions.Interception;
using Standalone.Cqrs.DependencyInjection.Autofac.Modules;
using Standalone.DependencyInjection.Adapters.Autofac.Options;

namespace Standalone.Cqrs.DependencyInjection.Autofac
{
    public sealed class StandaloneAutofacCqrsOptions : AutofacRootOptionsBase
    {
        public StandaloneAutofacCqrsOptions RegisterAssemblyHandlers(params Assembly[] assemblies)
        {
            var module = new StandaloneHandlersModule(assemblies);
            
            RegisterModule(module);

            return this;
        }

        public StandaloneAutofacCqrsOptions RegisterInterceptor<TInterceptor>()
            where TInterceptor : ISourceInterceptor
        {
            var module = new StandaloneInterceptorModule(typeof(TInterceptor));

            RegisterModule(module);

            return this;
        }
    }
}