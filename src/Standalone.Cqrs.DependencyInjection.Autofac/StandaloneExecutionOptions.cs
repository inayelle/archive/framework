using System;
using Standalone.Cqrs.Abstractions.Options;

namespace Standalone.Cqrs.DependencyInjection.Autofac
{
    internal sealed class StandaloneExecutionOptions : IExecutionOptions
    {
        public TimeSpan ExecutionTimeout { get; set; }
    }
}