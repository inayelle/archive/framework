using System;
using Autofac;
using Standalone.Cqrs.Abstractions.Interception;

namespace Standalone.Cqrs.DependencyInjection.Autofac.Modules
{
    internal sealed class StandaloneInterceptorModule : Module
    {
        private readonly Type[] _interceptorTypes;

        public StandaloneInterceptorModule(params Type[] interceptorTypes)
        {
            _interceptorTypes = interceptorTypes;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterTypes(_interceptorTypes)
               .As<ISourceInterceptor>();
        }
    }
}