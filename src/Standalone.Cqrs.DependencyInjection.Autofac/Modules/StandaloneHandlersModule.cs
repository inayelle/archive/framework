using System.Reflection;
using Autofac;
using Standalone.Cqrs.Abstractions.Handlers;
using Module = Autofac.Module;

namespace Standalone.Cqrs.DependencyInjection.Autofac.Modules
{
    internal sealed class StandaloneHandlersModule : Module
    {
        private readonly Assembly[] _assemblies;

        public StandaloneHandlersModule(Assembly[] assemblies)
        {
            _assemblies = assemblies;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(_assemblies)
               .AsClosedTypesOf(typeof(ICommandHandler<>))
               .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(_assemblies)
               .AsClosedTypesOf(typeof(ICommandHandler<,>))
               .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(_assemblies)
               .AsClosedTypesOf(typeof(IQueryHandler<,>))
               .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(_assemblies)
               .AsClosedTypesOf(typeof(IEventHandler<>))
               .AsImplementedInterfaces();
        }
    }
}