using System;
using Autofac;
using Standalone.Cqrs.Abstractions.Buses;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Options;
using Standalone.Cqrs.Abstractions.Pipelines;
using Standalone.Cqrs.Buses;
using Standalone.Cqrs.Context;
using Standalone.Cqrs.Pipelines;

namespace Standalone.Cqrs.DependencyInjection.Autofac.Modules
{
    internal sealed class StandaloneCqrsModule : Module
    {
        private readonly TimeSpan _executionTimeout;

        public StandaloneCqrsModule(TimeSpan executionTimeout)
        {
            _executionTimeout = executionTimeout;
        }

        protected override void Load(ContainerBuilder builder)
        {
            RegisterBuses(builder);
            RegisterPipelines(builder);

            builder.RegisterType<ExecutionContextBuilder>()
               .As<IExecutionContextBuilder>()
               .InstancePerDependency();
            
            builder.RegisterInstance(new StandaloneExecutionOptions
                {
                    ExecutionTimeout = _executionTimeout
                })
               .As<IExecutionOptions>()
               .SingleInstance();
        }

        private static void RegisterBuses(ContainerBuilder builder)
        {
            builder.RegisterType<CommandBus>()
               .As<ICommandBus>()
               .SingleInstance();

            builder.RegisterType<EventBus>()
               .As<IEventBus>()
               .SingleInstance();

            builder.RegisterType<QueryBus>()
               .As<IQueryBus>()
               .SingleInstance();
        }

        private static void RegisterPipelines(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(CommandExecutionPipeline<,>))
               .As(typeof(IExecutionPipeline<,>))
               .SingleInstance();

            builder.RegisterGeneric(typeof(CommandExecutionPipeline<>))
               .As(typeof(IExecutionPipeline<,>))
               .SingleInstance();

            builder.RegisterGeneric(typeof(QueryExecutionPipeline<,>))
               .As(typeof(IExecutionPipeline<,>))
               .SingleInstance();

            builder.RegisterGeneric(typeof(EventExecutionPipeline<>))
               .As(typeof(IExecutionPipeline<,>))
               .SingleInstance();
        }
    }
}