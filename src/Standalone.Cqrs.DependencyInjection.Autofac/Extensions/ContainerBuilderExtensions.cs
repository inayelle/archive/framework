using System;
using Autofac;
using Standalone.Cqrs.DependencyInjection.Autofac.Modules;

namespace Standalone.Cqrs.DependencyInjection.Autofac.Extensions
{
    public static class ContainerBuilderExtensions
    {
        public static ContainerBuilder RegisterCqrs(this ContainerBuilder builder,
            Action<StandaloneAutofacCqrsOptions> cqrs)
        {
            var cqrsOptions = new StandaloneAutofacCqrsOptions();

            cqrsOptions.RegisterModule(new StandaloneCqrsModule(TimeSpan.FromMinutes(50)));

            cqrs(cqrsOptions);

            cqrsOptions.ApplyModules(builder);

            return builder;
        }
    }
}