using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Context;

namespace Standalone.Cqrs.Abstractions.Interception
{
    public interface ISourceInterceptor
    {
        Task InterceptBefore(IExecutionContext context);

        Task InterceptAfter(IExecutionContext context);
    }
}