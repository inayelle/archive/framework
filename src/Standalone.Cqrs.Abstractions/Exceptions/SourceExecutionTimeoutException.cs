using System;

namespace Standalone.Cqrs.Abstractions.Exceptions
{
    public sealed class SourceExecutionTimeoutException : StandaloneCqrsExceptionBase
    {
        public Type SourceType { get; }
        public Type ResultType { get; }

        public TimeSpan TimeSpan { get; }

        public SourceExecutionTimeoutException(Type sourceType, Type resultType, TimeSpan timeSpan)
        {
            SourceType = sourceType;
            ResultType = resultType;
            TimeSpan = timeSpan;
        }
    }
}