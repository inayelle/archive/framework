using Standalone.Exceptional.Abstractions.Exceptions;

namespace Standalone.Cqrs.Abstractions.Exceptions
{
    public abstract class StandaloneCqrsExceptionBase : StandaloneExceptionBase
    {
    }
}