using System;

namespace Standalone.Cqrs.Abstractions.Exceptions
{
    public sealed class SourceHandlerNotFoundException : StandaloneCqrsExceptionBase
    {
        public Type SourceType { get; }
        public Type ResultType { get; }

        public SourceHandlerNotFoundException(Type sourceType, Type resultType)
        {
            SourceType = sourceType;
            ResultType = resultType;
        }
    }
}