namespace Standalone.Cqrs.Abstractions.Exceptions
{
    public sealed class IncompletedExecutionContextException : StandaloneCqrsExceptionBase
    {
    }
}