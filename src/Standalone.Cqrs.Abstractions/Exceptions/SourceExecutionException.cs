using System;

namespace Standalone.Cqrs.Abstractions.Exceptions
{
    public sealed class SourceExecutionException : StandaloneCqrsExceptionBase
    {
        public Type SourceType { get; }
        public Type ResultType { get; }

        public SourceExecutionException(Type sourceType, Type resultType)
        {
            SourceType = sourceType;
            ResultType = resultType;
        }
    }
}