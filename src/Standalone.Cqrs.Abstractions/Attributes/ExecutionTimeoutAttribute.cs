using System;

namespace Standalone.Cqrs.Abstractions.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class ExecutionTimeoutAttribute : Attribute
    {
        public TimeSpan Timeout { get; }

        public ExecutionTimeoutAttribute(int milliseconds)
        {
            Timeout = TimeSpan.FromMilliseconds(milliseconds);
        }
    }
}