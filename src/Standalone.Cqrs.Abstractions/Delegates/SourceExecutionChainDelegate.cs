using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;

namespace Standalone.Cqrs.Abstractions.Delegates
{
    public delegate SourceExecutionDelegate<TSource, TResult> SourceExecutionChainDelegate<TSource, TResult>(
        SourceExecutionDelegate<TSource, TResult> next)
        where TSource : ISource<TResult>
        where TResult : ISourceResult;
}