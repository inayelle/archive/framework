using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;

namespace Standalone.Cqrs.Abstractions.Delegates
{
    public delegate Task SourceExecutionDelegate<in TSource, TResult>(IExecutionContext<TSource, TResult> context)
        where TSource : ISource<TResult>
        where TResult : ISourceResult;
}