using System;

namespace Standalone.Cqrs.Abstractions.Options
{
    public interface IExecutionOptions
    {
        TimeSpan ExecutionTimeout { get; }
    }
}