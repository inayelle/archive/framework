using System.Threading;
using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;

namespace Standalone.Cqrs.Abstractions.Handlers
{
    public interface ICommandHandler<in TCommand, TResult>
        where TCommand : ICommand<TResult> 
        where TResult : ICommandResult
    {
        Task<TResult> Handle(TCommand command, CancellationToken ctoken);
    }

    public interface ICommandHandler<in TCommand>
        where TCommand : ICommand
    {
        Task Handle(TCommand command, CancellationToken ctoken);
    }
}