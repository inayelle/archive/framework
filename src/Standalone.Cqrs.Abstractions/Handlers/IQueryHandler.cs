using System.Threading;
using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;

namespace Standalone.Cqrs.Abstractions.Handlers
{
    public interface IQueryHandler<in TQuery, TResult>
        where TQuery : ISource<TResult>
        where TResult : IQueryResult
    {
        Task<TResult> Handle(TQuery query, CancellationToken ctoken);
    }
}