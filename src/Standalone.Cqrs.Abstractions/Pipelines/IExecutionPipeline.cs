using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;

namespace Standalone.Cqrs.Abstractions.Pipelines
{
    public interface IExecutionPipeline<in TSource, TResult> 
        where TSource : ISource<TResult>
        where TResult : ISourceResult
    {
        Task Execute(IExecutionContext<TSource, TResult> context);
    }
}