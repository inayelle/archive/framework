using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Abstractions.Delegates;

namespace Standalone.Cqrs.Abstractions.Pipelines
{
    public interface IExecutionMiddleware<TSource, TResult> 
        where TSource : ISource<TResult>
        where TResult : ISourceResult
    {
        Task Execute(IExecutionContext<TSource, TResult> context, SourceExecutionDelegate<TSource, TResult> next);
    }
}