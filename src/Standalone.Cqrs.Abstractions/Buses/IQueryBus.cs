using System;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;

namespace Standalone.Cqrs.Abstractions.Buses
{
    public interface IQueryBus
    {
        [Pure]
        Task<TResult> Perform<TQuery, TResult>(TQuery query, CancellationToken ctoken = default)
            where TQuery : IQuery<TResult>
            where TResult : IQueryResult;

        [Pure]
        Task<TResult> Perform<TQuery, TResult>(TQuery query, Guid correlationId, CancellationToken ctoken = default)
            where TQuery : IQuery<TResult>
            where TResult : IQueryResult;
    }
}