using System;
using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;

namespace Standalone.Cqrs.Abstractions.Buses
{
    public interface ICommandBus
    {
        Task Execute<TCommand>(TCommand command, CancellationToken ctoken = default) where TCommand : ICommand;

        Task Execute<TCommand>(TCommand command, Guid correlationId, CancellationToken ctoken = default)
            where TCommand : ICommand;

        [Pure]
        Task<TResult> Execute<TCommand, TResult>(TCommand command, CancellationToken ctoken = default)
            where TCommand : ICommand<TResult>
            where TResult : ICommandResult;

        [Pure]
        Task<TResult> Execute<TCommand, TResult>(TCommand command, Guid correlationId,
            CancellationToken ctoken = default)
            where TCommand : ICommand<TResult>
            where TResult : ICommandResult;
    }
}