using System;
using System.Threading;
using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Contracts.Sources;

namespace Standalone.Cqrs.Abstractions.Buses
{
    public interface IEventBus
    {
        Task Publish<TEvent>(TEvent @event, CancellationToken ctoken = default) where TEvent : IEvent;

        Task Publish<TEvent>(TEvent @event, Guid correlationId, CancellationToken ctoken = default)
            where TEvent : IEvent;
    }
}