using System;
using System.Threading;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.DependencyInjection.Abstractions;

namespace Standalone.Cqrs.Abstractions.Context
{
    public interface IExecutionContext : IDisposable
    {
        Guid CorrelationId { get; }

        Type SourceType { get; }
        Type ResultType { get; }

        CancellationTokenSource CancellationTokenSource { get; }
        CancellationToken CancellationToken => CancellationTokenSource.Token;

        IServiceScope ServiceScope { get; }

        TimeSpan Timeout { get; }
        
        ISource Source { get; }
        object Result { get; }
        Exception Exception { get; }

        bool IsCompleted { get; }
        bool IsCompletedSuccessfully { get; }
        bool IsCompletedFaulty { get; }

        void CompleteWithResult(object result);
        void CompleteWithException(Exception exception);
    }

    public interface IExecutionContext<out TSource, TResult> : IExecutionContext
        where TSource : ISource<TResult>
        where TResult : ISourceResult
    {
        Type IExecutionContext.SourceType => typeof(TSource);
        Type IExecutionContext.ResultType => typeof(TResult);

        new TSource Source { get; }
        new TResult Result { get; }

        ISource IExecutionContext.Source => Source;
        object IExecutionContext.Result => Result;

        void CompleteWithResult(TResult result);

        void IExecutionContext.CompleteWithResult(object result)
        {
            if (result?.GetType() != ResultType)
            {
                throw new ArgumentException(nameof(result));
            }

            CompleteWithResult((TResult) result);
        }
    }
}