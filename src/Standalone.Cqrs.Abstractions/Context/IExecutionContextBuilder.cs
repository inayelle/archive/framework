using System;
using System.Threading;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.DependencyInjection.Abstractions;

namespace Standalone.Cqrs.Abstractions.Context
{
    public interface IExecutionContextBuilder
    {
        IExecutionContextBuilder UseCancellationToken(CancellationToken ctoken);
        IExecutionContextBuilder UseCorrelationId(Guid correlationId);
        IExecutionContextBuilder UseServiceScope(IServiceScope serviceScope);
        
        IExecutionContext<TSource, TResult> Build<TSource, TResult>(TSource source)
            where TSource : ISource<TResult>
            where TResult : ISourceResult;
    }
}