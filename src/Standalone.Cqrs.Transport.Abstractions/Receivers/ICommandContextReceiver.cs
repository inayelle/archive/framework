namespace Standalone.Cqrs.Transport.Abstractions.Receivers
{
    public interface ICommandContextReceiver
    {
        void Start();
    }
}