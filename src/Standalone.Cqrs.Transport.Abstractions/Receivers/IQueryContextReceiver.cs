namespace Standalone.Cqrs.Transport.Abstractions.Receivers
{
    public interface IQueryContextReceiver
    {
        void Start();
    }
}