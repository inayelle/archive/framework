namespace Standalone.Cqrs.Transport.Abstractions.Receivers
{
    public interface IEventContextReceiver
    {
        void Start();
    }
}