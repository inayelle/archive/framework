using System;
using Standalone.Cqrs.Abstractions.Exceptions;

namespace Standalone.Cqrs.Transport.Abstractions.Exceptions
{
    public sealed class NoSuitableTransportFoundException : StandaloneCqrsExceptionBase
    {
        public Type SourceType { get; }
        public Type ResultType { get; }

        public NoSuitableTransportFoundException(Type sourceType, Type resultType)
        {
            SourceType = sourceType;
            ResultType = resultType;
        }
    }
}