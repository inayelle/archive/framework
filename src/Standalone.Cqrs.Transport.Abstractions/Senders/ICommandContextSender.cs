using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;

namespace Standalone.Cqrs.Transport.Abstractions.Senders
{
    public interface ICommandContextSender
    {
        void Start();
        
        Task Send<TCommand>(IExecutionContext<TCommand, VoidResult> context) where TCommand : ICommand;

        Task Send<TCommand, TResult>(IExecutionContext<TCommand, TResult> context)
            where TCommand : ICommand<TResult>
            where TResult : ICommandResult;
    }
}