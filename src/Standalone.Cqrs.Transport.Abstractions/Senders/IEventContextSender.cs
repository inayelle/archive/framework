using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;

namespace Standalone.Cqrs.Transport.Abstractions.Senders
{
    public interface IEventContextSender
    {
        void Start();
        void Stop();
        
        Task Publish<TEvent>(IExecutionContext<TEvent, VoidResult> context) where TEvent : IEvent;
    }
}