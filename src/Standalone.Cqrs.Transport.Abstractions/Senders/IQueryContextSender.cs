using System.Threading.Tasks;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;

namespace Standalone.Cqrs.Transport.Abstractions.Senders
{
    public interface IQueryContextSender
    {
        void Start();
        
        Task Send<TQuery, TResult>(IExecutionContext<TQuery, TResult> context)
            where TQuery : IQuery<TResult>
            where TResult : IQueryResult;
    }
}