using System;

namespace Standalone.Cqrs.Transport.Abstractions.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class SourceTransportAttribute : Attribute
    {
        public string TransportName { get; }

        public SourceTransportAttribute(string transportName)
        {
            TransportName = transportName ?? throw new ArgumentNullException(nameof(transportName));
        }
    }
}