using System;

namespace Standalone.Exceptional.Abstractions.Exceptions
{
    public abstract class StandaloneExceptionBase : ApplicationException
    {
        protected StandaloneExceptionBase()
        {
        }

        protected StandaloneExceptionBase(Exception innerException) : base(null, innerException)
        {
        }
    }
}