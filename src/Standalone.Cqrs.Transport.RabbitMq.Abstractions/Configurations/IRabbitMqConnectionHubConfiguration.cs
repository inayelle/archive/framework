using System.Collections.Generic;
using Standalone.Cqrs.Transport.RabbitMq.Abstractions.Options;

namespace Standalone.Cqrs.Transport.RabbitMq.Abstractions.Configurations
{
    public interface IRabbitMqConnectionHubConfiguration
    {
        IDictionary<string, RabbitMqTransportOptions> Connections { get; }
    }
}