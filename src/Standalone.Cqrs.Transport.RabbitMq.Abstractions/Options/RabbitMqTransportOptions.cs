using System;
using Standalone.Cqrs.Transport.Serialization.Abstractions;

namespace Standalone.Cqrs.Transport.RabbitMq.Abstractions.Options
{
    public sealed class RabbitMqTransportOptions
    {
        public string Host { get; set; } = "localhost";
        public int Port { get; set; } = 5672;

        public string VirtualHost { get; set; } = "/";

        public string User { get; set; } = "guest";
        public string Password { get; set; } = "guest";

        public IMessageSerializer MessageSerializer { get; }
        public IMessageDeserializer MessageDeserializer { get; }

        public RabbitMqTransportOptions(IMessageSerializer messageSerializer, IMessageDeserializer messageDeserializer)
        {
            MessageSerializer = messageSerializer ?? throw new ArgumentNullException(nameof(messageSerializer));
            MessageDeserializer = messageDeserializer ?? throw new ArgumentNullException(nameof(messageDeserializer));
        }
    }
}