using System;

namespace Standalone.Cqrs.Transport.RabbitMq.Abstractions.Envelopes
{
    public sealed class CommandRequestEnvelop
    {
        public Guid CorrelationId { get; set; }

        public Type CommandType { get; set; }
        public Type ResultType { get; set; }

        public byte[] Body { get; set; }
    }
}