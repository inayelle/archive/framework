using System;

namespace Standalone.Cqrs.Transport.RabbitMq.Abstractions.Envelopes
{
    public sealed class QueryResponseEnvelop
    {
        public Guid CorrelationId { get; set; }

        public Type ResultType { get; set; }
        
        public byte[] Body { get; set; }
    }
}