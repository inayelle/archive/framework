using System;

namespace Standalone.Cqrs.Transport.RabbitMq.Abstractions.Envelopes
{
    public sealed class EventEnvelop
    {
        public Guid CorrelationId { get; set; }

        public Type EventType { get; set; }

        public byte[] Body { get; set; }
    }
}