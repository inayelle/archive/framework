using System;

namespace Standalone.Cqrs.Transport.RabbitMq.Abstractions.Envelopes
{
    public sealed class QueryRequestEnvelop
    {
        public Guid CorrelationId { get; set; }

        public Type QueryType { get; set; }
        public Type ResultType { get; set; }

        public byte[] Body { get; set; }
    }
}