using System.Reflection;
using Standalone.Cqrs.DependencyInjection.Autofac;

namespace Standalone.Cqrs.Extensions.FluentValidation.Autofac
{
    public static class StandaloneCqrsOptionsExtensions
    {
        public static StandaloneAutofacCqrsOptions RegisterFluentValidation(this StandaloneAutofacCqrsOptions options,
            params Assembly[] assemblies)
        {
            options.RegisterInterceptor<SourceValidationInterceptor>();

            options.RegisterModule(new StandaloneCqrsValidationModule(assemblies));
            
            return options;
        }
    }
}