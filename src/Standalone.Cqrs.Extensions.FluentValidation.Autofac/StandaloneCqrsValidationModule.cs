using System.Reflection;
using Autofac;
using FluentValidation;
using Module = Autofac.Module;

namespace Standalone.Cqrs.Extensions.FluentValidation.Autofac
{
    internal sealed class StandaloneCqrsValidationModule : Module
    {
        private readonly Assembly[] _assemblies;

        public StandaloneCqrsValidationModule(Assembly[] assemblies)
        {
            _assemblies = assemblies;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(_assemblies)
               .AsClosedTypesOf(typeof(IValidator<>))
               .AsImplementedInterfaces();
        }
    }
}