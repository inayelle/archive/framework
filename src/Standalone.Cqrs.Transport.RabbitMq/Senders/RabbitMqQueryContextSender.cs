using System;
using System.Threading.Tasks;
using MassTransit;
using RabbitMQ.Client;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Transport.Abstractions.Senders;
using Standalone.Cqrs.Transport.RabbitMq.Abstractions.Envelopes;
using Standalone.Cqrs.Transport.RabbitMq.Abstractions.Options;
using Standalone.Cqrs.Transport.RabbitMq.Extensions;
using IMessageDeserializer = Standalone.Cqrs.Transport.Serialization.Abstractions.IMessageDeserializer;
using IMessageSerializer = Standalone.Cqrs.Transport.Serialization.Abstractions.IMessageSerializer;

namespace Standalone.Cqrs.Transport.RabbitMq.Senders
{
    internal sealed class RabbitMqQueryContextSender : IQueryContextSender
    {
        private readonly IBusControl _busControl;

        private readonly IMessageSerializer _serializer;
        private readonly IMessageDeserializer _deserializer;

        public RabbitMqQueryContextSender(RabbitMqTransportOptions options)
        {
            _serializer = options.MessageSerializer;
            _deserializer = options.MessageDeserializer;

            _busControl = Bus.Factory.CreateUsingRabbitMq(bus =>
            {
                bus.UseOriginator();

                bus.AutoStart = true;
                bus.Durable = false;
                bus.AutoDelete = true;
                bus.Exclusive = false;
                bus.ExchangeType = ExchangeType.Direct;

                bus.UseOptions(options);
            });
        }

        public void Start()
        {
            _busControl.Start();
        }

        public async Task Send<TQuery, TResult>(IExecutionContext<TQuery, TResult> context)
            where TQuery : IQuery<TResult> where TResult : IQueryResult
        {
            var requestEnvelop = new QueryRequestEnvelop
            {
                CorrelationId = context.CorrelationId,
                QueryType = context.SourceType,
                ResultType = context.ResultType,
                Body = _serializer.Serialize(context.Source)
            };

            var client = _busControl.CreateRequestClient<QueryRequestEnvelop>(context.Timeout);

            try
            {
                var response = await client.GetResponse<QueryResponseEnvelop>(requestEnvelop, context.CancellationToken, context.Timeout);
                var responseEnvelop = response.Message;

                var result = _deserializer.Deserialize(responseEnvelop.Body, responseEnvelop.ResultType);

                if (result is Exception exception)
                {
                    context.CompleteWithException(exception);
                }
                else
                {
                    context.CompleteWithResult(result);
                }
            }
            catch (Exception exception)
            {
                context.CompleteWithException(exception);
            }
        }
    }
}