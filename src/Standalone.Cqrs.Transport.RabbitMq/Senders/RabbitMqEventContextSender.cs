using System;
using System.Threading.Tasks;
using MassTransit;
using RabbitMQ.Client;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Transport.Abstractions.Senders;
using Standalone.Cqrs.Transport.RabbitMq.Abstractions.Envelopes;
using Standalone.Cqrs.Transport.RabbitMq.Abstractions.Options;
using Standalone.Cqrs.Transport.RabbitMq.Extensions;
using IMessageSerializer = Standalone.Cqrs.Transport.Serialization.Abstractions.IMessageSerializer;

namespace Standalone.Cqrs.Transport.RabbitMq.Senders
{
    internal sealed class RabbitMqEventContextSender : IEventContextSender, IDisposable, IAsyncDisposable
    {
        private readonly IBusControl _busControl;
        private readonly IMessageSerializer _messageSerializer;

        public RabbitMqEventContextSender(RabbitMqTransportOptions options)
        {
            _messageSerializer = options.MessageSerializer;
            _busControl = Bus.Factory.CreateUsingRabbitMq(bus =>
            {
                bus.ExchangeType = ExchangeType.Fanout;
                
                bus.UseOriginator();

                bus.AutoStart = true;
                bus.Durable = false;
                bus.AutoDelete = true;
                bus.Exclusive = false;
                bus.ExchangeType = ExchangeType.Fanout;
                
                bus.UseOptions(options);
            });
        }

        public void Start()
        {
            _busControl.Start();
        }

        public void Stop()
        {
            _busControl.Stop();
        }

        public async Task Publish<TEvent>(IExecutionContext<TEvent, VoidResult> context) where TEvent : IEvent
        {
            var body = _messageSerializer.Serialize(context.Source);

            var envelop = new EventEnvelop
            {
                CorrelationId = context.CorrelationId,
                EventType = context.SourceType,
                Body = body
            };

            try
            {
                await _busControl.Publish(envelop, context.CancellationToken);
            }
            catch (Exception exception)
            {
                context.CompleteWithException(exception);
            }
        }

        public void Dispose()
        {
            _busControl.Stop();
        }

        public async ValueTask DisposeAsync()
        {
            await _busControl.StopAsync();
        }
    }
}