using System;
using System.Reflection;
using System.Threading.Tasks;
using MassTransit;
using RabbitMQ.Client;
using Standalone.Cqrs.Abstractions.Buses;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Transport.Abstractions.Receivers;
using Standalone.Cqrs.Transport.RabbitMq.Abstractions.Envelopes;
using Standalone.Cqrs.Transport.RabbitMq.Abstractions.Options;
using Standalone.Cqrs.Transport.RabbitMq.Extensions;
using IMessageDeserializer = Standalone.Cqrs.Transport.Serialization.Abstractions.IMessageDeserializer;
using IMessageSerializer = Standalone.Cqrs.Transport.Serialization.Abstractions.IMessageSerializer;

namespace Standalone.Cqrs.Transport.RabbitMq.Receivers
{
    internal sealed class RabbitMqCommandContextReceiver : ICommandContextReceiver
    {
        private readonly ICommandBus _commandBus;
        private readonly IBusControl _busControl;
        private readonly IMessageSerializer _serializer;
        private readonly IMessageDeserializer _deserializer;

        public RabbitMqCommandContextReceiver(RabbitMqTransportOptions options, ICommandBus commandBus)
        {
            _serializer = options.MessageSerializer;
            _deserializer = options.MessageDeserializer;
            _commandBus = commandBus;

            _busControl = Bus.Factory.CreateUsingRabbitMq(bus =>
            {
                bus.UseOptions(options);
                
                bus.ExchangeType = ExchangeType.Direct;

                bus.ReceiveEndpoint(endpoint =>
                {
                    endpoint.AutoStart = true;
                    endpoint.Durable = false;
                    endpoint.AutoDelete = true;
                    endpoint.Exclusive = false;
                    endpoint.ExchangeType = ExchangeType.Direct;
                    
                    endpoint.Handler<CommandRequestEnvelop>(Consume);
                    endpoint.UseOriginatorFilter();
                });
            });
        }

        public void Start()
        {
            _busControl.Start();
        }

        private async Task Consume(ConsumeContext<CommandRequestEnvelop> context)
        {
            var requestEnvelop = context.Message;

            var command = _deserializer.Deserialize(requestEnvelop.Body, requestEnvelop.CommandType);

            object result;

            try
            {
                if (requestEnvelop.ResultType == typeof(VoidResult))
                {
                    await ExecuteCommandWithoutResult((dynamic) command, requestEnvelop.CorrelationId);
                    result = VoidResult.Instance;
                }
                else
                {
                    var methodInfo = typeof(RabbitMqCommandContextReceiver).GetMethod(nameof(ExecuteCommandWithResult),
                                                                                      BindingFlags.Instance | BindingFlags.NonPublic);

                    var method = methodInfo.MakeGenericMethod(requestEnvelop.CommandType, requestEnvelop.ResultType);

                    var task = (Task<object>) method.Invoke(this, new[]
                    {
                        command,
                        requestEnvelop.CorrelationId
                    });

                    result = await task;
                }
            }
            catch (Exception exception)
            {
                result = exception;
            }

            var responseEnvelop = new CommandResponseEnvelop
            {
                Body = _serializer.Serialize(result),
                ResultType = result.GetType(),
                CorrelationId = requestEnvelop.CorrelationId
            };

            await context.RespondAsync(responseEnvelop);
        }

        private async Task ExecuteCommandWithoutResult<TCommand>(TCommand command, Guid correlationId)
            where TCommand : ICommand
        {
            await _commandBus.Execute(command, correlationId);
        }

        private async Task<object> ExecuteCommandWithResult<TCommand, TResult>(TCommand command, Guid correlationId)
            where TCommand : ICommand<TResult>
            where TResult : ICommandResult
        {
            var result = await _commandBus.Execute<TCommand, TResult>(command, correlationId);

            return result;
        }
    }
}