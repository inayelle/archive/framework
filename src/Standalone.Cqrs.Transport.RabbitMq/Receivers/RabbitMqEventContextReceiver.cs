using System;
using System.Threading.Tasks;
using MassTransit;
using RabbitMQ.Client;
using Standalone.Cqrs.Abstractions.Buses;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Transport.Abstractions.Receivers;
using Standalone.Cqrs.Transport.RabbitMq.Abstractions.Envelopes;
using Standalone.Cqrs.Transport.RabbitMq.Abstractions.Options;
using Standalone.Cqrs.Transport.RabbitMq.Extensions;
using IMessageDeserializer = Standalone.Cqrs.Transport.Serialization.Abstractions.IMessageDeserializer;

namespace Standalone.Cqrs.Transport.RabbitMq.Receivers
{
    internal sealed class RabbitMqEventContextReceiver : IEventContextReceiver, IDisposable, IAsyncDisposable
    {
        private readonly IBusControl _busControl;
        private readonly IEventBus _eventBus;
        private readonly IMessageDeserializer _deserializer;

        public RabbitMqEventContextReceiver(IEventBus eventBus, RabbitMqTransportOptions options)
        {
            _eventBus = eventBus;
            _deserializer = options.MessageDeserializer;
            
            _busControl = Bus.Factory.CreateUsingRabbitMq(bus =>
            {
                bus.UseOptions(options);

                bus.ReceiveEndpoint(endpoint =>
                {
                    endpoint.AutoStart = true;
                    endpoint.Durable = false;
                    endpoint.AutoDelete = true;
                    endpoint.Exclusive = false;
                    endpoint.ExchangeType = ExchangeType.Fanout;
                    
                    endpoint.Handler<EventEnvelop>(Consume);
                    endpoint.UseOriginatorFilter();
                });
            });
        }

        public void Start()
        {
            _busControl.Start();
        }

        private async Task Consume(ConsumeContext<EventEnvelop> context)
        {
            var envelop = context.Message;

            if (envelop.EventType == null)
            {
                return;
            }

            if (!(_deserializer.Deserialize(envelop.Body, envelop.EventType) is IEvent @event))
            {
                return;
            }

            await PublishEvent((dynamic) @event, envelop.CorrelationId);
        }

        private Task PublishEvent<TEvent>(TEvent @event, Guid correlationId) where TEvent : IEvent
        {
            return _eventBus.Publish(@event, correlationId);
        }

        public void Dispose()
        {
            _busControl.Stop();
        }

        public async ValueTask DisposeAsync()
        {
            await _busControl.StopAsync();
        }
    }
}