using System;
using System.Reflection;
using System.Threading.Tasks;
using MassTransit;
using RabbitMQ.Client;
using Standalone.Cqrs.Abstractions.Buses;
using Standalone.Cqrs.Abstractions.Contracts.Results;
using Standalone.Cqrs.Abstractions.Contracts.Sources;
using Standalone.Cqrs.Transport.Abstractions.Receivers;
using Standalone.Cqrs.Transport.RabbitMq.Abstractions.Envelopes;
using Standalone.Cqrs.Transport.RabbitMq.Abstractions.Options;
using Standalone.Cqrs.Transport.RabbitMq.Extensions;
using IMessageDeserializer = Standalone.Cqrs.Transport.Serialization.Abstractions.IMessageDeserializer;
using IMessageSerializer = Standalone.Cqrs.Transport.Serialization.Abstractions.IMessageSerializer;

namespace Standalone.Cqrs.Transport.RabbitMq.Receivers
{
    internal sealed class RabbitMqQueryContextReceiver : IQueryContextReceiver
    {
        private readonly IBusControl _busControl;
        private readonly IQueryBus _queryBus;

        private readonly IMessageSerializer _serializer;
        private readonly IMessageDeserializer _deserializer;

        public RabbitMqQueryContextReceiver(RabbitMqTransportOptions options, IQueryBus queryBus)
        {
            _serializer = options.MessageSerializer;
            _deserializer = options.MessageDeserializer;
            _queryBus = queryBus;

            _busControl = Bus.Factory.CreateUsingRabbitMq(bus =>
            {
                bus.UseOptions(options);
                
                bus.ReceiveEndpoint(endpoint =>
                {
                    endpoint.Handler<QueryRequestEnvelop>(Consume);
                    endpoint.UseOriginatorFilter();

                    endpoint.AutoStart = true;
                    endpoint.Durable = false;
                    endpoint.AutoDelete = true;
                    endpoint.Exclusive = false;
                    endpoint.ExchangeType = ExchangeType.Direct;
                });
            });
        }

        public void Start()
        {
            _busControl.Start();
        }

        private async Task Consume(ConsumeContext<QueryRequestEnvelop> context)
        {
            var requestEnvelop = context.Message;

            var command = _deserializer.Deserialize(requestEnvelop.Body, requestEnvelop.QueryType);

            object result;

            try
            {
                var methodInfo = typeof(RabbitMqQueryContextReceiver).GetMethod(nameof(ExecuteQuery),
                                                                                BindingFlags.Instance | BindingFlags.NonPublic);

                var method = methodInfo.MakeGenericMethod(requestEnvelop.QueryType, requestEnvelop.ResultType);

                var task = (Task<object>) method.Invoke(this, new[]
                {
                    command,
                    requestEnvelop.CorrelationId
                });

                result = await task;
            }
            catch (Exception exception)
            {
                result = exception;
            }

            var responseEnvelop = new QueryResponseEnvelop
            {
                Body = _serializer.Serialize(result),
                ResultType = result.GetType(),
                CorrelationId = requestEnvelop.CorrelationId
            };

            await context.RespondAsync(responseEnvelop);
        }

        private async Task<object> ExecuteQuery<TQuery, TResult>(TQuery query, Guid correlationId)
            where TQuery : IQuery<TResult>
            where TResult : IQueryResult
        {
            return await _queryBus.Perform<TQuery, TResult>(query, correlationId);
        }
    }
}