using System.Reflection;
using System.Threading.Tasks;
using GreenPipes;
using MassTransit.RabbitMqTransport;

namespace Standalone.Cqrs.Transport.RabbitMq.Extensions
{
    internal static class RabbitMqReceiverEndpointConfigurationExtensions
    {
        internal static void UseOriginatorFilter(this IRabbitMqReceiveEndpointConfigurator configurator)
        {
            configurator.UseContextFilter(context =>
            {
                bool shouldConsume = true;

                if (context.Headers.TryGetHeader("Originator", out var header))
                {
                    shouldConsume = !header.Equals(Assembly.GetEntryAssembly()?.FullName);
                }

                return Task.FromResult(shouldConsume);
            });
        }
    }
}