using System.Reflection;
using GreenPipes;
using MassTransit;
using MassTransit.RabbitMqTransport;
using Standalone.Cqrs.Transport.RabbitMq.Abstractions.Options;

namespace Standalone.Cqrs.Transport.RabbitMq.Extensions
{
    internal static class RabbitMqBusFactoryConfiguratorExtensions
    {
        internal static void UseOptions(this IRabbitMqBusFactoryConfigurator configurator,
            RabbitMqTransportOptions options)
        {
            configurator.Host(options.Host, options.VirtualHost, rabbit =>
            {
                rabbit.Username(options.User);
                rabbit.Password(options.Password);
                rabbit.PublisherConfirmation = true;
            });
        }

        internal static void UseOriginator(this IRabbitMqBusFactoryConfigurator configurator)
        {
            configurator.ConfigureSend(sender =>
            {
                sender.UseExecute(context => 
                {
                    context.Headers.Set("Originator", Assembly.GetEntryAssembly()?.FullName);
                });
            });
        }
    }
}