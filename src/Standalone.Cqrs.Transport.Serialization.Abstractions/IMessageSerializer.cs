namespace Standalone.Cqrs.Transport.Serialization.Abstractions
{
    public interface IMessageSerializer
    {
        byte[] Serialize<TObject>(TObject message);
    }
}