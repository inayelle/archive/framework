using System;

namespace Standalone.Cqrs.Transport.Serialization.Abstractions
{
    public interface IMessageDeserializer
    {
        object Deserialize(byte[] bytes, Type objectType);
    }
}