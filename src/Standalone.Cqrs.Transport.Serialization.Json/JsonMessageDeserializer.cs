using System;
using System.Text;
using Newtonsoft.Json;
using Standalone.Cqrs.Transport.Serialization.Abstractions;

namespace Standalone.Cqrs.Transport.Serialization.Json
{
    public sealed class JsonMessageDeserializer : IMessageDeserializer
    {
        public object Deserialize(byte[] bytes, Type objectType)
        {
            var json = Encoding.UTF8.GetString(bytes);

            return JsonConvert.DeserializeObject(json, objectType);
        }
    }
}