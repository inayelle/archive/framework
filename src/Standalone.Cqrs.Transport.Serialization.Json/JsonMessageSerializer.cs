using System.Text;
using Newtonsoft.Json;
using Standalone.Cqrs.Transport.Serialization.Abstractions;

namespace Standalone.Cqrs.Transport.Serialization.Json
{
    public sealed class JsonMessageSerializer : IMessageSerializer
    {
        public byte[] Serialize<TMessage>(TMessage message)
        {
            var json = JsonConvert.SerializeObject(message);

            return Encoding.UTF8.GetBytes(json);
        }
    }
}