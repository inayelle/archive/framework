using System;
using MessagePack;
using MessagePack.Resolvers;
using Standalone.Cqrs.Transport.Serialization.Abstractions;

namespace Standalone.Cqrs.Transport.Serialization.MessagePack
{
    public sealed class MessagePackMessageDeserializer : IMessageDeserializer
    {
        private readonly MessagePackSerializerOptions _options;

        public MessagePackMessageDeserializer()
        {
            _options = ContractlessStandardResolver.Options;

            _options.WithResolver(ContractlessStandardResolver.Instance);
        }

        public object Deserialize(byte[] bytes, Type objectType)
        {
            return MessagePackSerializer.Deserialize(objectType, bytes, _options);
        }
    }
}