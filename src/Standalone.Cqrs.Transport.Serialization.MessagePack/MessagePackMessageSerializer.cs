using MessagePack;
using MessagePack.Resolvers;
using Standalone.Cqrs.Transport.Serialization.Abstractions;

namespace Standalone.Cqrs.Transport.Serialization.MessagePack
{
    public sealed class MessagePackMessageSerializer : IMessageSerializer
    {
        private readonly MessagePackSerializerOptions _options;

        public MessagePackMessageSerializer()
        {
            _options = ContractlessStandardResolver.Options;

            _options.WithResolver(ContractlessStandardResolver.Instance);
        }

        public byte[] Serialize<TObject>(TObject message)
        {
            return MessagePackSerializer.Serialize(message, _options);
        }
    }
}