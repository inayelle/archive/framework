using System;
using System.Text;
using Standalone.Cqrs.Transport.Serialization.Abstractions;
using YamlDotNet.Serialization;

namespace Standalone.Cqrs.Transport.Serialization.Yaml
{
    public sealed class YamlMessageDeserializer : IMessageDeserializer
    {
        private readonly Deserializer _deserializer;

        public YamlMessageDeserializer()
        {
            _deserializer = new Deserializer();
        }

        public object Deserialize(byte[] bytes, Type objectType)
        {
            var yaml = Encoding.UTF8.GetString(bytes);

            return _deserializer.Deserialize(yaml, objectType);
        }
    }
}