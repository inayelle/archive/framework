using System.Text;
using Standalone.Cqrs.Transport.Serialization.Abstractions;
using YamlDotNet.Serialization;

namespace Standalone.Cqrs.Transport.Serialization.Yaml
{
    public sealed class YamlMessageSerializer : IMessageSerializer
    {
        private readonly Serializer _serializer;

        public YamlMessageSerializer(Serializer serializer)
        {
            _serializer = serializer;
        }

        public byte[] Serialize<TObject>(TObject message)
        {
            var yaml = _serializer.Serialize(message);

            return Encoding.UTF8.GetBytes(yaml);
        }
    }
}