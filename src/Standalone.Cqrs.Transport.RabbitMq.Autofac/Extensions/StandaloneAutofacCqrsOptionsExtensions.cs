using System;
using MassTransit.Context;
using Standalone.Cqrs.DependencyInjection.Autofac;
using Standalone.Cqrs.Transport.RabbitMq.Abstractions.Envelopes;
using Standalone.Cqrs.Transport.RabbitMq.Autofac.Options;

namespace Standalone.Cqrs.Transport.RabbitMq.Autofac.Extensions
{
    public static class StandaloneAutofacCqrsOptionsExtensions
    {
        public static StandaloneAutofacCqrsOptions RegisterRabbitMq(this StandaloneAutofacCqrsOptions options,
            Action<StandaloneAutofacRabbitMqOptions> configure)
        {
            if (configure == null)
            {
                throw new ArgumentNullException(nameof(configure));
            }
            
            MessageCorrelation.UseCorrelationId<EventEnvelop>(envelop => envelop.CorrelationId);
            
            var rabbitMqOptions = new StandaloneAutofacRabbitMqOptions(options);
            
            configure(rabbitMqOptions);
            
            return options;
        }
    }
}