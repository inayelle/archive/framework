using Autofac;
using Standalone.Cqrs.Abstractions.Buses;
using Standalone.Cqrs.Transport.Abstractions.Receivers;
using Standalone.Cqrs.Transport.RabbitMq.Abstractions.Options;
using Standalone.Cqrs.Transport.RabbitMq.Receivers;

namespace Standalone.Cqrs.Transport.RabbitMq.Autofac.Modules.Receivers
{
    internal sealed class RabbitMqEventContextReceiverModule : Module
    {
        private readonly RabbitMqTransportOptions _transportOptions;

        public RabbitMqEventContextReceiverModule(RabbitMqTransportOptions transportOptions)
        {
            _transportOptions = transportOptions;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(context =>
                {
                    var eventBus = context.Resolve<IEventBus>();

                    var receiver = new RabbitMqEventContextReceiver(eventBus, _transportOptions);
                    
                    receiver.Start();

                    return receiver;
                })
               .As<IEventContextReceiver>()
               .SingleInstance()
               .AutoActivate();
        }
    }
}