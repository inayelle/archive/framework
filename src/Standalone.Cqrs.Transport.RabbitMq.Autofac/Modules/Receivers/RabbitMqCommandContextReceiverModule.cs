using Autofac;
using Standalone.Cqrs.Abstractions.Buses;
using Standalone.Cqrs.Transport.Abstractions.Receivers;
using Standalone.Cqrs.Transport.RabbitMq.Abstractions.Options;
using Standalone.Cqrs.Transport.RabbitMq.Receivers;

namespace Standalone.Cqrs.Transport.RabbitMq.Autofac.Modules.Receivers
{
    internal sealed class RabbitMqCommandContextReceiverModule : Module
    {
        private readonly RabbitMqTransportOptions _options;

        public RabbitMqCommandContextReceiverModule(RabbitMqTransportOptions options)
        {
            _options = options;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(context =>
                {
                    var commandBus = context.Resolve<ICommandBus>();

                    var receiver = new  RabbitMqCommandContextReceiver(_options, commandBus);
                    
                    receiver.Start();

                    return receiver;
                })
               .As<ICommandContextReceiver>()
               .SingleInstance()
               .AutoActivate();
        }
    }
}