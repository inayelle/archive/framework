using Autofac;
using Standalone.Cqrs.Abstractions.Buses;
using Standalone.Cqrs.Transport.Abstractions.Receivers;
using Standalone.Cqrs.Transport.RabbitMq.Abstractions.Options;
using Standalone.Cqrs.Transport.RabbitMq.Receivers;

namespace Standalone.Cqrs.Transport.RabbitMq.Autofac.Modules.Receivers
{
    internal sealed class RabbitMqQueryContextReceiverModule : Module
    {
        private readonly RabbitMqTransportOptions _options;

        public RabbitMqQueryContextReceiverModule(RabbitMqTransportOptions options)
        {
            _options = options;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(context =>
                {
                    var queryBus = context.Resolve<IQueryBus>();

                    var receiver = new RabbitMqQueryContextReceiver(_options, queryBus);
                    
                    receiver.Start();

                    return receiver;
                })
               .As<IQueryContextReceiver>()
               .SingleInstance()
               .AutoActivate();
        }
    }
}