using Autofac;
using Standalone.Cqrs.Transport.Abstractions.Senders;
using Standalone.Cqrs.Transport.RabbitMq.Abstractions.Options;
using Standalone.Cqrs.Transport.RabbitMq.Senders;

namespace Standalone.Cqrs.Transport.RabbitMq.Autofac.Modules.Senders
{
    internal sealed class RabbitMqEventContextSenderModule : Module
    {
        private readonly RabbitMqTransportOptions _options;

        public RabbitMqEventContextSenderModule(RabbitMqTransportOptions options)
        {
            _options = options;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(context =>
                {
                    var sender = new RabbitMqEventContextSender(_options);
                    
                    sender.Start();

                    return sender;
                })
               .As<IEventContextSender>()
               .SingleInstance()
               .AutoActivate();
        }
    }
}