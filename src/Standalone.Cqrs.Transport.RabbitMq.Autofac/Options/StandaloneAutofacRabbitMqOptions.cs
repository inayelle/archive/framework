using Standalone.Cqrs.DependencyInjection.Autofac;
using Standalone.Cqrs.Transport.RabbitMq.Abstractions.Options;
using Standalone.Cqrs.Transport.RabbitMq.Autofac.Modules.Receivers;
using Standalone.Cqrs.Transport.RabbitMq.Autofac.Modules.Senders;
using Standalone.DependencyInjection.Adapters.Autofac.Options;

namespace Standalone.Cqrs.Transport.RabbitMq.Autofac.Options
{
    public sealed class StandaloneAutofacRabbitMqOptions : AutofacNestedOptionsBase<StandaloneAutofacCqrsOptions>
    {
        public StandaloneAutofacRabbitMqOptions(StandaloneAutofacCqrsOptions baseOptions) : base(baseOptions)
        {
        }

        public StandaloneAutofacRabbitMqOptions RegisterEventSender(RabbitMqTransportOptions options)
        {
            var module = new RabbitMqEventContextSenderModule(options);
            
            RegisterModule(module);
            
            return this;
        }

        public StandaloneAutofacRabbitMqOptions RegisterEventReceiver(RabbitMqTransportOptions options)
        {
            var module = new RabbitMqEventContextReceiverModule(options);

            RegisterModule(module);

            return this;
        }

        public StandaloneAutofacRabbitMqOptions RegisterCommandSender(RabbitMqTransportOptions options)
        {
            var module = new RabbitMqCommandSenderModule(options);

            RegisterModule(module);

            return this;
        }

        public StandaloneAutofacRabbitMqOptions RegisterCommandReceiver(RabbitMqTransportOptions options)
        {
            var module = new RabbitMqCommandContextReceiverModule(options);

            RegisterModule(module);

            return this;
        }

        public StandaloneAutofacRabbitMqOptions RegisterQuerySender(RabbitMqTransportOptions options)
        {
            var module = new RabbitMqQueryContextSenderModule(options);

            RegisterModule(module);

            return this;
        }

        public StandaloneAutofacRabbitMqOptions RegisterQueryReceiver(RabbitMqTransportOptions options)
        {
            var module = new RabbitMqQueryContextReceiverModule(options);

            RegisterModule(module);

            return this;
        }
    }
}