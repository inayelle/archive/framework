using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Standalone.DependencyInjection.Adapters.Autofac")]
[assembly: InternalsVisibleTo("Standalone.DependencyInjection.Adapters.Microsoft")]