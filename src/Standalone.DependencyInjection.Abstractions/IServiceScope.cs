using System;
using System.Collections.Generic;

namespace Standalone.DependencyInjection.Abstractions
{
    public interface IServiceScope : IDisposable
    {
        TService GetService<TService>(bool isOptional = false) where TService : class;
        TService GetService<TService>(object key, bool isOptional = false) where TService : class;
        
        object GetService(Type serviceType, bool isOptional = false);

        bool HasService<TService>();
        bool HasService(Type serviceType);

        IReadOnlyCollection<TService> GetServices<TService>();
        IReadOnlyCollection<object> GetServices(Type serviceType);
    }
}