namespace Standalone.DependencyInjection.Abstractions
{
    public interface IServiceScopeFactory
    {
        IServiceScope Create();
    }
}