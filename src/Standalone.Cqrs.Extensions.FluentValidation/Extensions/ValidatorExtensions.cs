using System.Threading.Tasks;
using FluentValidation;

namespace Standalone.Cqrs.Extensions.FluentValidation.Extensions
{
    internal static class ValidatorExtensions
    {
        internal static async Task ValidateOrThrowAsync(this IValidator validator, object objectToValidate)
        {
            var result = await validator.ValidateAsync(objectToValidate).ConfigureAwait(false);

            if (result.IsValid == false)
            {
                throw new ValidationException(result.Errors);
            }
        }
    }
}