using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using FluentValidation;
using Standalone.Cqrs.Abstractions.Context;
using Standalone.Cqrs.Abstractions.Interception;
using Standalone.Cqrs.Extensions.FluentValidation.Extensions;
using Standalone.DependencyInjection.Abstractions;

namespace Standalone.Cqrs.Extensions.FluentValidation
{
    internal sealed class SourceValidationInterceptor : ISourceInterceptor
    {
        private delegate IValidator ValidatorFactory(IServiceScope serviceScope);

        private readonly ConcurrentDictionary<Type, ValidatorFactory> _validatorFactoryCache;

        public SourceValidationInterceptor()
        {
            _validatorFactoryCache = new ConcurrentDictionary<Type, ValidatorFactory>();
        }

        public async Task InterceptBefore(IExecutionContext context)
        {
            if (!_validatorFactoryCache.TryGetValue(context.SourceType, out var factory))
            {
                var validatorType = typeof(IValidator<>).MakeGenericType(context.SourceType);

                factory = serviceScope => (IValidator) serviceScope.GetService(validatorType, isOptional: true);

                _validatorFactoryCache.TryAdd(context.SourceType, factory);
            }

            var validator = factory(context.ServiceScope);

            if (validator == null)
            {
                return;
            }

            await validator.ValidateOrThrowAsync(context.Source);
        }

        public Task InterceptAfter(IExecutionContext context)
        {
            return Task.CompletedTask;
        }
    }
}