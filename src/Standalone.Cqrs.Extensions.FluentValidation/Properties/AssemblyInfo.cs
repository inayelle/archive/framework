using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Standalone.Cqrs.Extensions.FluentValidation.Autofac")]
[assembly: InternalsVisibleTo("Standalone.Cqrs.Extensions.FluentValidation.Microsoft")]