using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace Standalone.Cqrs.Abstractions.Contracts.Results
{
    [StructLayout(LayoutKind.Sequential, Size = 1)]
    public sealed class VoidResult : ICommandResult, IEventResult
    {
        public static VoidResult Instance { get; } = new VoidResult();

        public static Task<VoidResult> CompletedTask => Task.FromResult(Instance);

        private VoidResult()
        {
        }
    }
}