namespace Standalone.Cqrs.Abstractions.Contracts.Results
{
    public interface ICommandResult : IQueryResult
    {
    }
}