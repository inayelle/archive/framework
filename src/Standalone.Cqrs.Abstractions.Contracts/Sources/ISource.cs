using System;
using Standalone.Cqrs.Abstractions.Contracts.Results;

namespace Standalone.Cqrs.Abstractions.Contracts.Sources
{
    public interface ISource
    {
        Type ResultType { get; }
    }
    
    public interface ISource<TResult> : ISource where TResult : ISourceResult
    {
        Type ISource.ResultType => typeof(TResult);
    }
}