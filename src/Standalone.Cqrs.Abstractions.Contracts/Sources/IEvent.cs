using System;
using Standalone.Cqrs.Abstractions.Contracts.Results;

namespace Standalone.Cqrs.Abstractions.Contracts.Sources
{
    public interface IEvent : ISource<VoidResult>
    {
        Type ISource.ResultType => typeof(VoidResult);
    }
}