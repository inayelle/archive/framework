using System;
using Standalone.Cqrs.Abstractions.Contracts.Results;

namespace Standalone.Cqrs.Abstractions.Contracts.Sources
{
    public interface IQuery<TResult> : ISource<TResult> where TResult : IQueryResult
    {
        Type ISource.ResultType => typeof(TResult);
    }
}