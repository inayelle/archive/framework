using System;
using Standalone.Cqrs.Abstractions.Contracts.Results;

namespace Standalone.Cqrs.Abstractions.Contracts.Sources
{
    public interface ICommand<TResult> : ISource<TResult> where TResult : ICommandResult
    {
        Type ISource.ResultType => typeof(TResult);
    }

    public interface ICommand : ICommand<VoidResult>
    {
        Type ISource.ResultType => typeof(VoidResult);
    }
}